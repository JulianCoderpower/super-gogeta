# Fuuuuusion!

Goku and Vegeta try to fuse with one another to get stronger!

Fusion is a function which has two arguments: "goku" and "vegeta".

"goku" and "vegeta" are two strings.

You have to merge the strings, remove all double characters and finally sort them alphabetically.

example:


var goku = 'aaaaaccccceeeeeee';

var vegeta = 'bbbbbbbdddddddfffff';

fusion (goku, vegeta) = 'abcdef';



