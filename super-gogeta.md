# Fuuuuusion!

Goku and Vegeta try to fuse together to become stronger!

Fusion is a function which have two arguments "goku" and "vegeta".

"goku" and "vegeta" are two strings.

You have to merge the strings in one without duplicated characters sort in alphabetical order.

example:


goku = 'aaaaaccccceeeeeee';

vegeta = 'bbbbbbbdddddddfffff';

fusion = abcdef;


SPECS

Must return a string

Must return a string sort in alphabetic order

Must return a string with goku and vegeta merged