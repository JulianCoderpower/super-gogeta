var expect = require('expect.js');
var fusion = require('../sources/fusion.js');

suite('Must return a string', function(){

    test('FU', function(){
        var goku = 'ihavetosavetheuniverse';
        var vegeta = 'iwanttobethestrongestmanoftheuniverse';
        var result = fusion(goku, vegeta);
        expect(result).to.be.a('string');
    });

});

suite('Must return a string sort in alphabetic order', function(){

    test('SI', function(){
        var goku = 'eabcpq';
        var vegeta = 'ogfzxw';
        var result = fusion(goku, vegeta);
        expect(result).to.be('abcefgopqwxz');
    });

});

suite('Must return a string with goku and vegeta merged', function(){

    test('ON!', function(){
        var goku = 'strongpeopledontputothersdowntheyliftthemup';
        var vegeta = 'yourtrainingisntoverwhenyourbodygivesupbutwhenyourmindgivesup';
        var result = fusion(goku, vegeta);
        expect(result).to.be.a('string');
        expect(result).to.be('abdefghilmnoprstuvwy');

        var goku2 = 'vivamussuscipittortoregetfelisporttitorvolutpatsedporttitorl2ectusnibhmaurisblanditaliqueteliteget88tinciduntnibhpulvinaracurabiturnonnullasitametnisltempusconvallisquisaclectusdonecsollicitudinmolestiemalesuadavestibulumanteipsumprimisinfaucibusorciluctusetultricesposuerecubiliacuraedonecvelitnequeauctorsitametaliquamvelullamcorpersitametligulasedportt1itorlectusnibhquisquevelitnisipretiumutlaciniainelementumidenimsedportti99torlectusnibhcurabiturnonnullasitametnisltempusconvallisquisaclectus';
        var vegeta2 = 'vivamussuscipittortoregetfelisporttitorvolutpatsedporttitorlectusnibhmaurisblanditaliqueteli2tegettinciduntnibhpulvinaracurabitur1nonnullasitametnisltempusconvallisquisa9clectusdonecsollicitudinmolestiemalesuadavestibulumanteipsumprimisinfaucibusorciluctusetultricesposuerecubiliacuraedonecvelitnequeauctorsitametaliquamvelullamcorpersitametligulasedporttitorlectusnibhquisquevelitnisipretiumutlaciniainelementumidenimsedporttitorlectusnibhcurabiturnonnullasitametnisltempusconvallisquisaclectus';
        var result2 = fusion(goku2, vegeta2);
        expect(result2).to.be.a('string');
        expect(result2).to.be('1289abcdefghilmnopqrstuv');

    });

});