module.exports = function fusion(goku , vegeta){
    var array = goku.concat(vegeta).split("");
    var gogeta = [];
    array.reduce(function(prev, current){
        if(gogeta.indexOf(current) === -1)
            gogeta.push(current);
    }, array[0]);

    return gogeta.sort().join("");
};